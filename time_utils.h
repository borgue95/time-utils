#ifndef UTILS_TIMEUTILS_H
#define UTILS_TIMEUTILS_H

#include <time.h>

typedef struct TimeSpent {
    struct timespec start;
    struct timespec end;
    double in_secs;
    double in_msecs;
    double in_usecs;
} TimeSpent;

TimeSpent* initTime();
void destroyTime(TimeSpent *elapsedTime);
void start_counting_time(TimeSpent *data);
void stop_counting_time(TimeSpent *data);
double compute_time_in_secs(TimeSpent *data);
double compute_time_in_msecs(TimeSpent *data);
double compute_time_in_usecs(TimeSpent *data);
double get_time_in_secs(TimeSpent *data);
double get_time_in_msecs(TimeSpent *data);
double get_time_in_usecs(TimeSpent *data);

#endif //UTILS_TIMEUTILS_H
