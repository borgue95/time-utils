#include "time_utils.h"

#include <stdlib.h>
#include <time.h>

TimeSpent* initTime()
{
    TimeSpent* elapsedTime = calloc(sizeof(TimeSpent),1);
    return elapsedTime;
}

void destroyTime(TimeSpent *elapsedTime)
{
    free(elapsedTime);
}

void start_counting_time(TimeSpent *data)
{
    clock_gettime(CLOCK_MONOTONIC, &(data->start));
}

void stop_counting_time(TimeSpent *data)
{
    clock_gettime(CLOCK_MONOTONIC, &(data->end));
    data->in_secs = compute_time_in_secs(data);
    data->in_msecs = compute_time_in_msecs(data);
    data->in_usecs = compute_time_in_usecs(data);
}

double compute_time_in_secs(TimeSpent *data)
{
    double secs;
    secs = data->end.tv_sec - data->start.tv_sec;
    secs += (data->end.tv_nsec - data->start.tv_nsec) / 1e9;
    return secs;
}

double compute_time_in_msecs(TimeSpent *data)
{
    double msecs;
    msecs = (data->end.tv_sec - data->start.tv_sec) * 1e3;
    msecs += (data->end.tv_nsec - data->start.tv_nsec) / 1e6;
    return msecs;
}

double compute_time_in_usecs(TimeSpent *data)
{
    double usecs;
    usecs = (data->end.tv_sec - data->start.tv_sec) * 1e6;
    usecs += (data->end.tv_nsec - data->start.tv_nsec) / 1e3;
    return usecs;
}

double get_time_in_secs(TimeSpent *data)
{
    return data->in_secs;
}

double get_time_in_msecs(TimeSpent *data)
{
    return data->in_msecs;
}

double get_time_in_usecs(TimeSpent *data)
{
    return data->in_usecs;
}
