# Time Utils

A very little interface to measure time in C/C++ programs. As simple as this:

```c
TimeSpent *right_now = initTime();
start_counting_time(right_now);
// some code to test
stop_counting_time(right_now);
printf("Time elapsed: %.2lf\n", get_time_in_msecs(right_now));
destroyTime(right_now);
```

It supports nested measurements if using different instances. You can get the time in seconds, milliseconds and microseconds.

## Compile

```bash
git clone https://gitlab.com/borgue95/time-utils.git
cd time-utils
make
sudo make install
```

This will install the library in your system. In the future, I may improve the installation process.

## Use

Add this parameters to the compile parameters (`gcc`): 

* `-L/usr/lib/time_utils -ltimeutils`

Add this include to your program: 

* `#include <time_utils/time_utils.h>`
